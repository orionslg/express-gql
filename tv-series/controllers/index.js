const Series = require("../models/Series");

class Controller {
  static async createSeries(req, res, next) {
    try {
      const db = req.db;
      const collection = db.collection("Series");
      const { title, overview, poster_path, popularity, tags } = req.body;
      const data = {
        title,
        overview,
        poster_path,
        popularity,
        tags,
      };

      const result = await Series.create(collection, data);

      res.status(201).json({
        Series: result.ops[0],
      });
    } catch (err) {
      next(err);
    }
  }

  static async findAll(req, res, next) {
    try {
      const db = req.db;
      const collection = db.collection("Series");
      const result = await Series.findAll(collection);
      res.status(200).json({
        Series: result,
      });
    } catch (err) {
      next(err);
    }
  }

  static async findOne(req, res, next) {
    try {
      const db = req.db;
      const collection = db.collection("Series");
      const result = await Series.findOne({ collection, id: req.params.id });
      if (result) {
        res.status(200).json({
          Series: result,
        });
      } else {
        res.status(404).json({
          message: "Not found",
        });
      }
    } catch (err) {
      next(err);
    }
  }

  static async remove(req, res, next) {
    try {
      const db = req.db;
      const collection = db.collection("Series");
      const series = await Series.findOne({ collection, id: req.params.id });
      if (series) {
        await Series.remove({ collection, id: req.params.id });

        res.status(200).json({
          message: "Series deleted successfully",
          status: 200,
        });
      } else {
        res.status(404).json({
          message: "Not found",
          status: 404,
        });
      }
    } catch (err) {
      next(err);
    }
  }

  static async update(req, res, next) {
    try {
      const db = req.db;
      const collection = db.collection("Series");
      let series = await Series.findOne({ collection, id: req.params.id });
      const { title, overview, poster_path, popularity, tags } = req.body;
      const id = req.params.id;
      const data = {
        id,
        tags,
        title,
        overview,
        popularity,
        poster_path,
      };
      if (series) {
        await Series.update({ collection, data });
        series = await Series.findOne({ collection, id: req.params.id });
        res.status(200).json({
          series: series,
        });
      } else {
        res.status(404).json({
          message: "Not found",
          status: 404,
        });
      }
    } catch (err) {
      next(err);
    }
  }
}

module.exports = Controller;
