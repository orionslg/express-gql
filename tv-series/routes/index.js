const router = require("express").Router();
const Series = require("./tv-series");

router.use("/series", Series);

module.exports = router;
