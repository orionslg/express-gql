const router = require("express").Router();
const Controller = require("../controllers");

router.post("/", Controller.createSeries);
router.get("/", Controller.findAll);
router.get("/:id", Controller.findOne);
router.delete("/:id", Controller.remove);
router.put("/:id", Controller.update);

module.exports = router;
