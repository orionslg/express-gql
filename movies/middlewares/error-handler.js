const errorHandler = (err, req, res, _) => {
  let status = 500;
  let errors = [];
  const obj = {
    status,
    errors,
    message: "",
  };

  console.error(err);

  switch (err.name) {
    case "MongoError":
      obj.message = "Bad Request";
      status = 400;
      obj.status = status;
      obj.errors.push(err.message);
      res.status(status).json(obj);
      break;

    default:
      status = err.status || 500;
      obj.status = status;
      obj.message = err.name || "Internal Server Error";
      obj.errors.push(err.message);
      res.status(status).json(obj);
      break;
  }
};

module.exports = errorHandler;
