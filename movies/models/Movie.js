const { ObjectID } = require("mongodb");

class Movie {
  static create(collection, data) {
    return collection.insertOne({
      title: data.title,
      overview: data.overview,
      poster_path: data.poster_path,
      popularity: data.popularity,
      tags: data.tags,
    });
  }

  static findAll(collection) {
    return collection.find({}).toArray();
  }

  static findOne({ collection, id }) {
    return collection.findOne({ _id: ObjectID(id) });
  }

  static remove({ collection, id }) {
    return collection.deleteOne({ _id: ObjectID(id) });
  }

  static update({ collection, data }) {
    const { id, title, overview, popularity, tags, poster_path } = data;

    return collection.updateOne(
      { _id: ObjectID(id) },
      {
        $set: { title, overview, popularity, tags, poster_path },
      }
    );
  }
}

module.exports = Movie;
