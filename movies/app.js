const express = require("express");
const dotenv = require("dotenv");
const { MongoClient } = require("mongodb");
const routes = require("./routes");
const errorHandler = require("./middlewares/error-handler");

if (process.env.NODE_ENV === "development") {
  dotenv.config();
}

const PORT = process.env.PORT || 4001;
const app = express();
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const client = new MongoClient(process.env.URL, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
});

(async function () {
  try {
    await client.connect();
    const db = client.db(process.env.DB_NAME);

    app.use((req, res, next) => {
      req.db = db;
      next();
    });

    app.use(routes);
    app.use(errorHandler);
    app.listen(PORT, () => {
      console.log(`Listening on PORT ${PORT}`);
    });
  } catch (err) {
    console.error(err);
  }
})();
