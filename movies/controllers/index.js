const Movies = require("../models/Movie");

class Controller {
  static async createMovie(req, res, next) {
    try {
      const db = req.db;
      const collection = db.collection("Movies");
      const { title, overview, poster_path, popularity, tags } = req.body;
      const data = {
        title,
        overview,
        poster_path,
        popularity,
        tags,
      };

      const result = await Movies.create(collection, data);

      res.status(201).json({
        movies: result.ops[0],
      });
    } catch (err) {
      next(err);
    }
  }

  static async findAll(req, res, next) {
    try {
      const db = req.db;
      const collection = db.collection("Movies");
      const result = await Movies.findAll(collection);
      res.status(200).json({
        movies: result,
      });
    } catch (err) {
      next(err);
    }
  }

  static async findOne(req, res, next) {
    try {
      const db = req.db;
      const collection = db.collection("Movies");
      const result = await Movies.findOne({ collection, id: req.params.id });
      if (result) {
        res.status(200).json({
          movie: result,
        });
      } else {
        res.status(404).json({
          message: "Not found",
        });
      }
    } catch (err) {
      next(err);
    }
  }

  static async remove(req, res, next) {
    try {
      const db = req.db;
      const collection = db.collection("Movies");
      const movie = await Movies.findOne({ collection, id: req.params.id });
      if (movie) {
        await Movies.remove({ collection, id: req.params.id });

        res.status(200).json({
          message: "Movie deleted successfully",
          status: 200,
        });
      } else {
        res.status(404).json({
          message: "Not found",
          status: 404,
        });
      }
    } catch (err) {
      next(err);
    }
  }

  static async update(req, res, next) {
    try {
      const db = req.db;
      const collection = db.collection("Movies");
      let movie = await Movies.findOne({ collection, id: req.params.id });
      const { title, overview, poster_path, popularity, tags } = req.body;
      const id = req.params.id;
      const data = {
        id,
        tags,
        title,
        overview,
        popularity,
        poster_path,
      };

      if (movie) {
        await Movies.update({ collection, data });
        movie = await Movies.findOne({ collection, id: req.params.id });
        res.status(200).json({
          movie: movie,
        });
      } else {
        res.status(404).json({
          message: "Not found",
          status: 404,
        });
      }
    } catch (err) {
      next(err);
    }
  }
}

module.exports = Controller;
