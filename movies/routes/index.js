const router = require("express").Router();
const Movie = require("./movie");

router.use("/movies", Movie);

module.exports = router;
