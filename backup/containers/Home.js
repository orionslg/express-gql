import React from "react";
import { Container, CardDeck } from "react-bootstrap";
import MovieCard from "../components/MovieCard";

export default function Home() {
  return (
    <>
      <Container>
        <CardDec>
          <MovieCard />
          <MovieCard />
          <MovieCard />
        </CardDec>
      </Container>
    </>
  );
}
