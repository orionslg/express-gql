import React, { Fragment } from "react";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "@apollo/react-hooks";
import TodoList from "./TodoList";
import AddTodoForm from "./AddTodoForm";
import "bootstrap/dist/css/bootstrap.min.css";

const client = new ApolloClient({
  url: "http://localhost:4000",
});

function App() {
  return (
    <ApolloProvider client={client}>
      <Fragment>
        <AddTodoForm />
        <TodoList />
      </Fragment>
    </ApolloProvider>
  );
}

export default App;
