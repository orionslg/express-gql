import React from "react";

function TodoItem({ todo }) {
  return (
    <>
      <h1>{todo.title}</h1>
      <p>{todo.description}</p>
    </>
  );
}

export default TodoItem;
