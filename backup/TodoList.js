import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import TodoItem from "./TodoItem";

const TODOS = gql`
  {
    todos {
      id
      title
      description
    }
  }
`;

function TodoList() {
  const { loading, error, data } = useQuery(TODOS);
  if (loading) return <p> Loading ...</p>;
  if (error) return <p> Error </p>;

  return data.todos.map((todo) => <TodoItem key={todo.id} todo={todo} />);
}

export default TodoList;
