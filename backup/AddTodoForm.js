import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

const TODOS = gql`
  {
    todos {
      id
      title
      description
    }
  }
`;

const ADD_TODO = gql`
  mutation($title: String, $description: String) {
    addTodo(ittle: $title, description: $description) {
      id
      title
      description
    }
  }
`;

function AddTodoForm() {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [addTodo] = useMutation(ADD_TODO, {
    update(cache, { data: { addTodo } }) {
      const { todos } = cache.readQuery({ query: TODOS });
      cache.writeQuery({
        query: TODOS,
        data: { todos: todos.concat([addTodo]) },
      });
    },
  });

  const handleOnSubmit = (event) => {
    event.preventDefault();
    addTodo({ variables: { title: title, description: description } });

    setTitle("");
    setDescription("");
  };
  return (
    <form onSubmit={handleOnSubmit}>
      <input
        type="text"
        value={title}
        onChange={(event) => setTitle(event.target.value)}
      />
      <input
        type="text"
        value={description}
        onChange={(event) => setDescription(event.target.value)}
      />
    </form>
  );
}

export default AddTodoForm;
