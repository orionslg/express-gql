import React from "react";
import { Container, Row } from "react-bootstrap";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import MovieCard from "../components/MovieCard.jsx";
import AddForm from "../components/MovieAddForm.jsx";

const MOVIES = gql`
  {
    movies {
      _id
      title
      overview
      poster_path
      popularity
      tags
    }
  }
`;

export default function Home() {
  const { loading, error, data } = useQuery(MOVIES);
  if (loading) return <p>loading ...</p>;
  if (error) return <p> error ... </p>;
  return (
    <>
      <Container className="mt-5">
        <AddForm />
        <Row className="mt-5 align-items-center">
          {data.movies.map((el) => (
            <MovieCard key={el._id} movie={el} />
          ))}
        </Row>
      </Container>
    </>
  );
}
