import React, { useState, useCallback } from "react";
import { Container, Row, Form, Button } from "react-bootstrap";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { useParams } from "@reach/router";
import { Link } from "@reach/router";
import checkboxes from "../components/tags.js";
import CheckBox from "../components/CheckBox.jsx";
import EditForm from "../components/EditMovie.jsx";

const GET_MOVIE = gql`
  query($id: ID!) {
    getOneMovie(id: $id) {
      _id
      title
      overview
      poster_path
      popularity
      tags
    }
  }
`;

export default function detail() {
  const params = useParams();

  const { loading, error, data } = useQuery(GET_MOVIE, {
    variables: { id: params.id },
  });

  console.log(data);

  console.log("Errors:", error);

  if (loading) return <h1>loading...</h1>;

  const movie = data.getOneMovie;

  if (error) return <h1>Error...</h1>;

  return (
    <>
      <Link to="/">Back Home</Link>
      <Container className="mt-5">
        <EditForm movie={movie} />
      </Container>
    </>
  );
}
