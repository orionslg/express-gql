import React from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import { Link } from "@reach/router";

export default function Home() {
  return (
    <>
      <Container
        className="mt-5"
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div>
          <h1>Welcome</h1>
          <Row>
            <Col md={12}>
              <Link to="/movies">
                <Button variant="success" style={{ width: "100%" }}>
                  Movies
                </Button>
              </Link>
            </Col>
          </Row>
          <Row className="mt-2">
            <Col md={12}>
              <Link to="/series">
                <Button variant="primary" style={{ width: "100%" }}>
                  Series
                </Button>
              </Link>
            </Col>
          </Row>
        </div>
      </Container>
    </>
  );
}
