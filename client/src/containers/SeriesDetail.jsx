import React, { useState, useCallback } from "react";
import { Container, Row, Form, Button } from "react-bootstrap";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { useParams } from "@reach/router";
import { Link } from "@reach/router";
import EditForm from "../components/EditSeries.jsx";

const GET_SERIES = gql`
  query($id: ID!) {
    getOneSeries(id: $id) {
      _id
      title
      overview
      poster_path
      popularity
      tags
    }
  }
`;

export default function detail() {
  const params = useParams();

  const { loading, error, data } = useQuery(GET_SERIES, {
    variables: { id: params.id },
  });

  if (loading) return <h1>loading...</h1>;

  const series = data.getOneSeries;

  if (error) return <h1>Error...</h1>;

  return (
    <>
      <Link to="/">Back Home</Link>
      <Container className="mt-5">
        <EditForm series={series} />
      </Container>
    </>
  );
}
