import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import SeriesCard from "../components/SeriesCard.jsx";
import AddForm from "../components/SeriesAddForm.jsx";

const SERIES = gql`
  {
    series {
      _id
      title
      overview
      poster_path
      popularity
      tags
    }
  }
`;

export default function Home() {
  const { loading, error, data } = useQuery(SERIES);

  if (loading) return <p>loading ...</p>;
  if (error) return <p> error ... </p>;
  return (
    <>
      <Container className="mt-5">
        <Row>
          <Col md={12}>
            <AddForm />
          </Col>
        </Row>
        <Row className="mt-5 align-items-center">
          {data.series.map((el) => (
            <Col key={el._id} sm={6} md={4} xs={12}>
              <SeriesCard key={el._id} serie={el} />
            </Col>
          ))}
        </Row>
      </Container>
    </>
  );
}
