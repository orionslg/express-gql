import React from "react";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import { Link } from "@reach/router";

export default function Navbars() {
  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Link to="/">
        <Navbar.Brand>Home</Navbar.Brand>
      </Link>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto">
          <Link to="/movies">
            <div className="nav-link">Movies</div>
          </Link>
          <Link to="/series">
            <div className="nav-link">Series</div>
          </Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
