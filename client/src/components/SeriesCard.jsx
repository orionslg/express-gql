import React from "react";
import { Card, Button } from "react-bootstrap";
import { Link, useNavigate } from "@reach/router";
import { useMutation } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

const REMOVE_SERIES = gql`
  mutation($id: ID!) {
    deleteSeries(id: $id) {
      status
      message
    }
  }
`;

const SERIES = gql`
  {
    series {
      _id
      title
      overview
      poster_path
      popularity
      tags
    }
  }
`;

export default function SeriesCard({ serie }) {
  const navigate = useNavigate();
  const [deleteSeries] = useMutation(REMOVE_SERIES, {
    update(cache, { data: { deleteSeries } }) {
      const { series } = cache.readQuery({ query: SERIES });
      console.log(series);
      const newSeries = [...series].filter((el) => el._id !== serie._id);
      cache.writeQuery({
        query: SERIES,
        data: { series: newSeries },
      });
    },
  });

  function removeSeries() {
    console.log("masuk di sini");
    deleteSeries({
      variables: {
        id: serie._id,
      },
    });
  }

  return (
    <>
      <Card className="mt-5" style={{ width: "18rem" }}>
        <Card.Img
          style={{ height: "27rem" }}
          variant="top"
          src={serie.poster_path}
        />
        <Card.Body>
          <Card.Title>{serie.title}</Card.Title>
          <Card.Text>
            <h6>Popularity: {serie.popularity}</h6>
          </Card.Text>
          <Button
            variant="primary"
            onClick={() => navigate(`detail/series/${serie._id}`)}
            className="mr-3"
          >
            See Details
          </Button>
          <Button onClick={() => removeSeries()} variant="light">
            Delete
          </Button>
        </Card.Body>
      </Card>
    </>
  );
}
