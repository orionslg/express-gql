const checkboxes = [
  {
    name: "Action",
    key: "Action",
    label: "Action",
  },
  {
    name: "Romance",
    key: "Romance",
    label: "Romance",
  },
  {
    name: "Comedy",
    key: "Comedy",
    label: "Comedy",
  },
  {
    name: "Sci-Fi",
    key: "Sci-Fi",
    label: "Sci-Fi",
  },
];

export default checkboxes;
