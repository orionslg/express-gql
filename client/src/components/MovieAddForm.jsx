import React, { useState, useCallback } from "react";
import { useMutation } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { Form, Button } from "react-bootstrap";
import CheckBox from "./CheckBox.jsx";
import checkboxes from "./tags.js";

const ADD_MOVIE = gql`
  mutation(
    $title: String!
    $overview: String!
    $poster_path: String!
    $popularity: Float!
    $tags: [String]!
  ) {
    addMovie(
      input: {
        title: $title
        overview: $overview
        poster_path: $poster_path
        popularity: $popularity
        tags: $tags
      }
    ) {
      _id
      title
      overview
      poster_path
      popularity
      tags
    }
  }
`;

const MOVIES = gql`
  {
    movies {
      _id
      title
      overview
      poster_path
      popularity
      tags
    }
  }
`;

export default function AddForm() {
  const [title, setTitle] = useState("");
  const [overview, setOverview] = useState("");
  const [poster, setPoster] = useState("");
  const [popularity, setPopularity] = useState("");
  const [tags, setTags] = useState(new Map());
  const [addMovie] = useMutation(ADD_MOVIE, {
    update(cache, { data: { addMovie } }) {
      const { movies } = cache.readQuery({ query: MOVIES });
      cache.writeQuery({
        query: MOVIES,
        data: { movies: movies.concat([addMovie]) },
      });
    },
  });
  const handleOnSubmit = (event) => {
    console.log(title, overview, poster, popularity, [...tags.keys()]);
    event.preventDefault();
    let tagsArr = [...tags.keys()];
    addMovie({
      variables: {
        title: title,
        overview: overview,
        poster_path: poster,
        popularity: +popularity,
        tags: tagsArr,
      },
    });
    setTitle("");
    setOverview("");
    setPoster("");
    setPopularity("");
    setTags(new Map());
  };

  const handleChange = useCallback(({ target: { name, checked } }) => {
    setTags((prevState) => {
      return new Map(prevState).set(name, checked);
    });
  }, []);

  return (
    <>
      <Form onSubmit={handleOnSubmit}>
        <Form.Group controlId="formTitle">
          <Form.Label>Title</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter title"
            value={title}
            onChange={(event) => setTitle(event.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="formOverview">
          <Form.Label>Overview</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter overview"
            value={overview}
            onChange={(event) => setOverview(event.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="formPoster">
          <Form.Label>Poster URL</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter poster url"
            value={poster}
            onChange={(event) => setPoster(event.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="formPopularity">
          <Form.Label>Popularity</Form.Label>
          <Form.Control
            type="number"
            value={popularity}
            onChange={(event) => setPopularity(event.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="formTags">
          <Form.Label>Tags: </Form.Label>
          {checkboxes.map((item) => (
            <label className="mr-1 ml-1" key={item.key}>
              {item.name}
              <CheckBox
                name={item.name}
                checked={tags.get(item.name)}
                onChange={handleChange}
              />
            </label>
          ))}
        </Form.Group>
        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>
    </>
  );
}
