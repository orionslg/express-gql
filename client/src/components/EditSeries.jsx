import React, { useState, useCallback } from "react";
import { Container, Row, Form, Button } from "react-bootstrap";
import { useMutation } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { useParams, redirectTo, Link } from "@reach/router";
import checkboxes from "./tags.js";
import CheckBox from "./CheckBox.jsx";

const EDIT_SERIES = gql`
  mutation(
    $id: ID!
    $title: String!
    $overview: String!
    $poster_path: String!
    $popularity: Float!
    $tags: [String]!
  ) {
    updateSeries(
      input: {
        id: $id
        title: $title
        overview: $overview
        poster_path: $poster_path
        popularity: $popularity
        tags: $tags
      }
    ) {
      _id
      title
      overview
      poster_path
      popularity
      tags
    }
  }
`;

export default function EditForm({ series }) {
  const params = useParams();
  const id = series._id;
  const [title, setTitle] = useState(series.title);
  const [overview, setOverview] = useState(series.overview);
  const [poster, setPoster] = useState(series.poster_path);
  const [popularity, setPopularity] = useState(+series.popularity);
  const [tags, setTags] = useState(new Map());

  const [updateSeries, { loading }] = useMutation(EDIT_SERIES, {});

  const handleOnSubmit = (event) => {
    event.preventDefault();
    let tagsArr = [...tags.keys()];

    updateSeries({
      variables: {
        id: id,
        title: title,
        overview: overview,
        poster_path: poster,
        popularity: +popularity,
        tags: tagsArr,
      },
    });
  };

  if (loading) {
    redirectTo("/series");
  }
  const handleChange = useCallback(({ target: { name, checked } }) => {
    setTags((prevState) => {
      return new Map(prevState).set(name, checked);
    });
  }, []);

  return (
    <>
      <Container className="mt-5">
        <Form onSubmit={handleOnSubmit}>
          <Form.Group controlId="formTitle">
            <Form.Label>Title</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter title"
              defaultValue={series.title}
              onChange={(event) => setTitle(event.target.value)}
            />
          </Form.Group>

          <Form.Group controlId="formOverview">
            <Form.Label>Overview</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter overview"
              defaultValue={series.overview}
              onChange={(event) => setOverview(event.target.value)}
            />
          </Form.Group>

          <Form.Group controlId="formPoster">
            <Form.Label>Poster URL</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter poster url"
              defaultValue={series.poster_path}
              onChange={(event) => setPoster(event.target.value)}
            />
          </Form.Group>

          <Form.Group controlId="formPopularity">
            <Form.Label>Popularity</Form.Label>
            <Form.Control
              type="number"
              defaultValue={series.popularity}
              onChange={(event) => setPopularity(event.target.value)}
            />
          </Form.Group>

          <Form.Group controlId="formTags">
            <Form.Label>Tags: </Form.Label>
            {checkboxes.map((item) => (
              <label className="mr-1 ml-1" key={item.key}>
                {item.name}
                <CheckBox
                  name={item.name}
                  checked={tags.get(item.name)}
                  onChange={handleChange}
                />
              </label>
            ))}
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </Container>
    </>
  );
}
