import React from "react";
import { Card, Button } from "react-bootstrap";
import { Link, useNavigate } from "@reach/router";
import { useMutation } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

const REMOVE_MOVIE = gql`
  mutation($id: ID!) {
    deleteMovie(id: $id) {
      status
      message
    }
  }
`;

const MOVIES = gql`
  {
    movies {
      _id
      title
      overview
      poster_path
      popularity
      tags
    }
  }
`;

export default function MovieCard({ movie }) {
  const navigate = useNavigate();
  const [deleteMovie] = useMutation(REMOVE_MOVIE, {
    update(cache, { data: { deleteMovie } }) {
      const { movies } = cache.readQuery({ query: MOVIES });
      const newMovies = [...movies].filter((el) => el._id !== movie._id);
      cache.writeQuery({
        query: MOVIES,
        data: { movies: newMovies },
      });
    },
  });

  function removeMovie() {
    deleteMovie({
      variables: {
        id: movie._id,
      },
    });
  }

  return (
    <>
      <Card className="mr-3 ml-3 mt-5" style={{ width: "18rem" }}>
        <Card.Img
          style={{ height: "27rem" }}
          variant="top"
          src={movie.poster_path}
        />
        <Card.Body>
          <Card.Title>{movie.title}</Card.Title>
          <Card.Text>Popularity: {movie.popularity}</Card.Text>
          <Button
            variant="primary"
            onClick={() => navigate(`detail/movies/${movie._id}`)}
            className="mr-3"
          >
            See Details
          </Button>
          <Button onClick={() => removeMovie()} variant="light">
            Delete
          </Button>
        </Card.Body>
      </Card>
    </>
  );
}
