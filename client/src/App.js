import React, { Fragment } from "react";
import ReactDOM from "react-dom";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "@apollo/react-hooks";
import { Router, Link } from "@reach/router";
import "bootstrap/dist/css/bootstrap.min.css";
import Home from "./containers/Home.jsx";
import MovieDetail from "./containers/MovieDetail.jsx";
import Movies from "./containers/Movies.jsx";
import Series from "./containers/Series.jsx";
import SeriesDetail from "./containers/SeriesDetail.jsx";
import Navbars from "./components/Navbar.jsx";
const client = new ApolloClient({
  uri: "http://localhost:4000",
});

const App = () => {
  return (
    <ApolloProvider client={client}>
      <Fragment>
        <Navbars />
        <Router>
          <Home path="/" />
          <Movies path="/movies" />
          <Series path="/series" />
          <MovieDetail path="detail/movies/:id" />
          <SeriesDetail path="detail/series/:id" />
        </Router>
      </Fragment>
    </ApolloProvider>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
