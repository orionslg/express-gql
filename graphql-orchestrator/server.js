const { ApolloServer, gql } = require("apollo-server");
const dotenv = require("dotenv");
const typeDefs = require("./schema");
const resolvers = require("./resolvers");

if (process.env.NODE_ENV === "development") {
  dotenv.config();
}

const server = new ApolloServer({ typeDefs, resolvers });

server.listen().then(({ url }) => {
  console.log("GraphQL server running on ", url);
});
