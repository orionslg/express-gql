const Redis = require("ioredis");

module.exports = new Redis({ port: 6379, host: "localhost" });
