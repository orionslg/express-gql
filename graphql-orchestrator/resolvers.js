const axios = require("axios");
const redis = require("./redis");

module.exports = {
  Query: {
    movies: async () => {
      try {
        let movies = await redis.get("movies");
        if (movies) {
          movies = JSON.parse(movies);
        } else {
          const { data } = await axios.get("http://localhost:4001/movies");
          movies = data.movies;
          await redis.set("movies", JSON.stringify(movies));
        }

        return movies;
      } catch (err) {
        return err;
      }
    },

    series: async () => {
      try {
        let series = await redis.get("series");
        if (series) {
          series = JSON.parse(series);
        } else {
          const { data } = await axios.get("http://localhost:4002/series");
          series = data.Series;
          await redis.set("series", JSON.stringify(series));
        }
        return series;
      } catch (err) {
        return err.response;
      }
    },

    getOneMovie: async (_, { id }) => {
      try {
        let movies = await redis.get("movies");
        if (movies) {
          movies = JSON.parse(movies);
          let [movie] = movies.filter((el) => el._id === id);
          if (movie) return movie;
        } else {
          const { data } = await axios.get(
            `http://localhost:4001/movies/${id}`
          );
          return data.movie;
        }
      } catch (err) {
        return err;
      }
    },

    getOneSeries: async (_, { id }) => {
      try {
        let series = await redis.get("series");
        if (series) {
          series = JSON.parse(series);
          let [serie] = series.filter((el) => el._id === id);
          if (serie) return serie;
        } else {
          const { data } = await axios.get(
            `http://localhost:4002/series/${id}`
          );
          return data.Series;
        }
      } catch (err) {
        return err;
      }
    },
  },

  Mutation: {
    addMovie: async (_, { input }) => {
      try {
        const { data } = await axios({
          method: "POST",
          url: "http://localhost:4001/movies",
          data: input,
        });
        await redis.del("movies");
        return data.movies;
      } catch (err) {
        return err.response;
      }
    },

    addSeries: async (_, { input }) => {
      try {
        const { data } = await axios({
          method: "POST",
          url: "http://localhost:4002/series",
          data: input,
        });
        await redis.del("series");
        return data.Series;
      } catch (err) {
        return err.response;
      }
    },

    deleteMovie: async (_, { id }) => {
      try {
        const { data } = await axios({
          method: "DELETE",
          url: `http://localhost:4001/movies/${id}`,
        });
        await redis.del("movies");
        return data;
      } catch (err) {
        return err.response;
      }
    },

    deleteSeries: async (_, { id }) => {
      try {
        console.log("masuk di deleteSeries", id);
        const { data } = await axios({
          method: "DELETE",
          url: `http://localhost:4002/series/${id}`,
        });
        await redis.del("series");
        return data;
      } catch (err) {
        return err.response;
      }
    },

    updateMovie: async (_, { input }) => {
      try {
        const { data } = await axios({
          method: "PUT",
          url: `http://localhost:4001/movies/${input.id}`,
          data: input,
        });
        await redis.del("movies");
        return data.movie;
      } catch (err) {
        return err.response;
      }
    },

    updateSeries: async (_, { input }) => {
      try {
        const { data } = await axios({
          method: "PUT",
          url: `http://localhost:4002/series/${input.id}`,
          data: input,
        });
        await redis.del("series");
        return data.series;
      } catch (err) {
        return err.response;
      }
    },
  },
};
