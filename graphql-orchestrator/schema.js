const { gql } = require("apollo-server");

const typeDefs = gql`
  type Movie {
    _id: ID!
    title: String!
    overview: String!
    poster_path: String!
    popularity: Float!
    tags: [String]!
  }

  type Series {
    _id: ID!
    title: String!
    overview: String!
    poster_path: String!
    popularity: Float!
    tags: [String]!
  }

  input NewMovieInput {
    title: String!
    overview: String!
    poster_path: String!
    popularity: Float!
    tags: [String]!
  }

  input NewSeriesInput {
    title: String!
    overview: String!
    poster_path: String!
    popularity: Float!
    tags: [String]!
  }

  input UpdateMovieInput {
    id: ID!
    title: String!
    overview: String!
    poster_path: String!
    popularity: Float!
    tags: [String]!
  }

  input UpdateSeriesInput {
    id: ID!
    title: String!
    overview: String!
    poster_path: String!
    popularity: Float!
    tags: [String]!
  }

  type Query {
    movies: [Movie]!
    series: [Series]!
    getOneMovie(id: ID!): Movie!
    getOneSeries(id: ID!): Series!
  }

  type Response {
    status: Int
    message: String
  }
  type Mutation {
    addMovie(input: NewMovieInput!): Movie!
    updateMovie(input: UpdateMovieInput!): Movie!
    deleteMovie(id: ID!): Response!
    addSeries(input: NewSeriesInput!): Series!
    updateSeries(input: UpdateSeriesInput!): Series!
    deleteSeries(id: ID!): Response!
  }
`;

module.exports = typeDefs;
